/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ood;

import java.util.ArrayList;

/**
 *
 * @author dev
 */
public class NonResidentTaxPayer {
    private String tfn;
    private String name;
    private double income;
    private String country;

    public NonResidentTaxPayer(String tfn, String name, double income, String country) {
        this.tfn = tfn;
        this.name = name;
        this.income = income;
        this.country = country;
    }
    
    
    private double calcTax() {
        double rate = 0.33;
        return rate;
    }
    
    void print() {
        double rate = this.calcTax();
        rate = rate * 100;
        
        System.out.println("Tax Details for Resident:");
        System.out.println("--------------------------");
        System.out.println();
        System.out.println("Tax File Number: " + this.tfn);
        System.out.println("Name: " + this.name);
        System.out.println("Income: " + this.income);
        System.out.println("Country: " + this.country);
        System.out.println("Current Tax Rate: "+ rate + "%");
        System.out.println();

    }
    static void printObject(ArrayList<NonResidentTaxPayer> nonresidents) {
          // Print non-residents object
            System.out.println("Non-Residents in Object: "); 		
            System.out.println("==============================="); 		
            System.out.println(); 		

            for (int counter = 0; counter < nonresidents.size(); counter++) { 
                    NonResidentTaxPayer a = nonresidents.get(counter);
                    a.print();		
            }   
            
    }

    @Override
    public String toString() {
        return "NonResidentTaxPayer{" + "tfn=" + tfn + ", name=" + name + ", income=" + income + ", country=" + country + '}';
    }
    
    
}
