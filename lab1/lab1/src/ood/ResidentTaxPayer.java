/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ood;

import java.util.ArrayList;

/**
 *
 * @author dev
 */
public class ResidentTaxPayer {
    
    private String tfn;
    private String name;
    private double income;
    private String state;

    public ResidentTaxPayer(String tfn, String name, double income, String state) {
        this.tfn = tfn;
        this.name = name;
        this.income = income;
        this.state = state;
    }
    
    private double calcTax() {
        
        double rate;
        
        if (this.income <= 20000) {    
            rate = 0;
            return  rate;
        }
        else {
            rate = 0.3;
            return rate;
        }
    }
    
    void print() {
        double rate = this.calcTax();
        rate = rate * 100;
        
        System.out.println("Tax Details for Resident:");
        System.out.println("--------------------------");
        System.out.println();
        System.out.println("Tax File Number: " + this.tfn);
        System.out.println("Name: " + this.name);
        System.out.println("Income: " + this.income);
        System.out.println("State: " + this.state);
        System.out.println("Current Tax Rate: "+ rate + "%");
        System.out.println();


    
    }
           static void printObject(ArrayList<ResidentTaxPayer> residents) {
             // Print residents object
            System.out.println("Residents in Object: "); 		
            System.out.println("==============================="); 
            System.out.println(); 		


            for (int counter = 0; counter < residents.size(); counter++) { 
                    ResidentTaxPayer a = residents.get(counter);
                    a.print();
            }        
        }

    @Override
    public String toString() {
        return "ResidentTaxPayer{" + "tfn=" + tfn + ", name=" + name + ", income=" + income + ", state=" + state + '}';
    }
           
         
    
}
