/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ood;

import java.util.*;

/**
 *
 * @author dev
 */
public class Lab1 {
        ArrayList<ResidentTaxPayer> residents = new ArrayList<ResidentTaxPayer>();   
        ArrayList<NonResidentTaxPayer> nonresidents = new ArrayList<NonResidentTaxPayer>();   

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
            
            Lab1 workaround = new Lab1();
        //Init Residents
            ResidentTaxPayer res1 = new ResidentTaxPayer("234234234234", "Bob", 119000, "NSW");
            ResidentTaxPayer res2 = new ResidentTaxPayer("7943984389489", "Carol", 12000, "QLD");
            ResidentTaxPayer res3 = new ResidentTaxPayer("8375458438453", "BAr", 19000, "VIC");
            ResidentTaxPayer res4 = new ResidentTaxPayer("4543488920002", "Foo", 325453, "SA");
        //Init arraylist with residents
                workaround.residents.add(res1);
                workaround.residents.add(res2);
                workaround.residents.add(res3);
                workaround.residents.add(res4);
                
        // Init nonresidents
            NonResidentTaxPayer nores = new NonResidentTaxPayer("83579384579384", "Michael", 23423428, "USA");
            NonResidentTaxPayer nores1 = new NonResidentTaxPayer("5938459734994", "D", 677777745, "USA");
            NonResidentTaxPayer nores2 = new NonResidentTaxPayer("9283246823873", "Name", 56456, "USA");
            NonResidentTaxPayer nores3 = new NonResidentTaxPayer("6468324682347", "Another Name", 45645, "USA");
            
        // Init araylist with residents
                workaround.nonresidents.add(nores1);
                workaround.nonresidents.add(nores2);
                workaround.nonresidents.add(nores3);
       
              //Object 
              ArrayList<Object> obj = new ArrayList<Object>();
              obj.add(nores);
              obj.add(res1);
        // Printing
            
        System.out.print(obj);
            
    }
    

}
