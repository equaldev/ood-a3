/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment.pkg1;

import assignment.pkg1.categories.Category;

/**
 *
 * @author dev
 */
public class Purchase {
    int receipt_id;
    int card_id;
    int time;
    Category cat;

    public Purchase(int receipt_id, int card_id, int time, Category cat) {
        this.receipt_id = receipt_id;
        this.card_id = card_id;
        this.time = time;
        this.cat = cat;
    }

    @Override
    public String toString() {
        return "Purchase";
    }

    
    
}
