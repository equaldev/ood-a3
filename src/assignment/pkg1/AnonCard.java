/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment.pkg1;


public class AnonCard extends Card {
    
    public AnonCard(int id,int points) {
        super.modifier = 0.01;
        super.id = id;
        super.points = points;
    }

    @Override
    public String toString() {
        return "Anon";
    }
    
    
}
