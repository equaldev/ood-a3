/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment.pkg1.categories;


public class Pet extends Category {
    
     public Pet(String name, int amount) {
        super.amount = amount;
        super.name = name;
        this.toString();
    }
     
    @Override
    public String toString() {
        return "Pet";
    }
    
}
