/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment.pkg1.categories;

// A "Category" of item in the shop.

public class Food extends Category {

    public Food(String name, int amount) {
        super.amount = amount;
        super.name = name;
        this.toString();

    }
    
    
    @Override
    public String toString() {
        return "Food";
    }
    
}
