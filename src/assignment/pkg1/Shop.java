/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment.pkg1;

import assignment.pkg1.categories.Health;
import assignment.pkg1.categories.Hardware;
import assignment.pkg1.categories.Garden;
import assignment.pkg1.categories.Pet;
import assignment.pkg1.categories.Food;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author dev
 */
public class Shop {
    ArrayList<Card> cards = new ArrayList<Card>();
    ArrayList<Purchase> purchases = new ArrayList<Purchase>();
    private static String userChoice;
    private static Scanner input = new Scanner(System.in);   

    public Shop() {
    }
    
    // Initialise cards
    AnonCard card = new AnonCard(1, 0);
    PremiumCard card1 = new PremiumCard("Brenna", "dfdsfs", 123.21, 2, 574);
    PremiumCard card4 = new PremiumCard("ASJD", "asffasfas", 1227.33, 3, 1500);
    BasicCard card2 = new BasicCard("Other", "afas", 1000.00, 4, 0);
    AnonCard card3 = new AnonCard(5, 567);
    AnonCard card5 = new AnonCard(6, 10);
    PremiumCard card6 = new PremiumCard("sgsdg", "sglkns", 6722, 7, 574);
    PremiumCard card7 = new PremiumCard("kshjsf", "afllls", 2000, 8, 0);
    PremiumCard card12 = new PremiumCard("kshjsf", "afllls", 100, 8, 2329);
    BasicCard card8 = new BasicCard("sdgsg", "sdg", 2100.12, 9, 26423);
    AnonCard card9 = new AnonCard(12, 282);
    BasicCard card10 = new BasicCard("alskf", "lkn", 1219, 10, 2712);
    BasicCard card11 = new BasicCard("lsdjbjsd", "hhfgh", 80, 11, 234);

    
    // initalise categories of items in shop.
    Food bread = new Food("Bread", 2);
    Food milk = new Food("Milk", 5);
    Food eggs = new Food("Eggs", 3);
    Food steak = new Food("Steak", 18);
    Food chicken = new Food("Chicken", 12);
    Food juice = new Food("Juice", 5);
    Food tea = new Food("Tea", 4);
    Food coffee = new Food("Coffee", 8);
    
    Hardware nails = new Hardware("Nails", 100);
    Hardware hammer = new Hardware("Hammer", 25);
    Hardware drill = new Hardware("Drill", 1000);
    Hardware shovel = new Hardware("Shovel", 12);
    Hardware pick = new Hardware("Pickaxe", 39);
    Hardware toolbox = new Hardware("Toolbox", 50);
    Hardware hose = new Hardware("Hose", 20);
    
    Health panadol = new Health("Panadol", 20);
    Health soap = new Health("Soap", 5);
    Health bandaid = new Health("Band-Aid", 12);
    Health medicine = new Health("Cough Medicine", 18);
    Health lozenge = new Health("Throat Lozenge", 2);
    
    Pet shampoo = new Pet("Shampoo", 8);
    Pet brush = new Pet("Brush", 15);
    Pet food = new Pet("Cat Food", 20);
    Pet fish = new Pet("Fish Food", 5);
    Pet seed = new Pet("Bird Seed", 12);
    
    Garden tomato = new Garden("Tomato Seed", 5);
    Garden potato = new Garden("Potato Seed", 11);
    Garden sberry = new Garden("Strawberry Seed", 6);
    Garden leek = new Garden("Leek Seed", 8);
    Garden flower = new Garden("Cyclamen", 14);
    
    // Add purchases to shop.
    Purchase p1 = new Purchase(123, 2, 12, bread);
    Purchase p2 = new Purchase(564, 11, 10, milk);
    Purchase p3 = new Purchase(756, 4, 7, eggs);
    Purchase p4 = new Purchase(252, 6, 4, nails);
    Purchase p5 = new Purchase(143, 5, 6, hammer);
    Purchase p6 = new Purchase(741, 2, 3, drill);
    Purchase p7 = new Purchase(942, 11, 18, lozenge);
    Purchase p8 = new Purchase(235, 10, 18, shovel);
    Purchase p9 = new Purchase(567, 8, 18, pick);
    Purchase p10 = new Purchase(211, 6, 18, toolbox);
    Purchase p11 = new Purchase(111, 7, 18, hose);
    Purchase p12 = new Purchase(698, 4, 18, coffee);
    Purchase p13 = new Purchase(163, 9, 18, tea);
    Purchase p14 = new Purchase(416, 3, 18, juice);
    Purchase p15 = new Purchase(835, 2, 18, steak);
    Purchase p16 = new Purchase(262, 5, 18, chicken);
    Purchase p17 = new Purchase(927, 10, 18, panadol);
    Purchase p18 = new Purchase(947, 11, 18, bandaid);
    Purchase p19 = new Purchase(974, 7, 18, medicine);
    Purchase p20 = new Purchase(924, 4, 18, soap);
    Purchase p21 = new Purchase(963, 5, 18, shampoo);
    Purchase p22 = new Purchase(984, 7, 18, brush);
    Purchase p23 = new Purchase(932, 4, 18, food);
    Purchase p24 = new Purchase(955, 3, 18, fish);
    Purchase p25 = new Purchase(988, 8, 18, seed);
    Purchase p26 = new Purchase(923, 11, 18, brush);
    Purchase p27 = new Purchase(925, 4, 18, tomato);
    Purchase p28 = new Purchase(986, 8, 18, potato);
    Purchase p29 = new Purchase(934, 9, 18, sberry);
    Purchase p30 = new Purchase(965, 6, 18, leek);
    Purchase p31 = new Purchase(955, 4, 18, flower);

    
    
    
// Performs purchases to load simple data in dev.
    public void populateDev() {
        
 //Test Anon card with drill ($1000) @ 0 points
        // Should return 10 points.
        // this.makePurchase(p6, card);
        
//Test Basic card with nails ($100) @ 0 points
        //Balance: 1000.   
        // Should return 2 points.
        //this.makePurchase(p4, card2);
        //Balance: 0.   
        // Should return 1 point.
        //this.makePurchase(p4, card2);
             
//Test Premium card with drill ($100) @ 0 points

        //Balance: 2000.   
        //Purchase Amount: 1000.   
        // Should return 30 points.
        //this.makePurchase(p6, card7);
        
        //Balance: 2000   
        //Purchase Amount: 39 (pick)   
        // Should return 0 points. (only whole points are made)
        this.makePurchase(p9, card7);

    }
    
    // Performs purchases to insert heaps of random data.
     public void populate() {
        
        this.makePurchase(p7, card);
        this.makePurchase(p1, card4);
        this.makePurchase(p2, card4);
        this.makePurchase(p3, card3);
        this.makePurchase(p4, card2);
        this.makePurchase(p5, card4);
        this.makePurchase(p7, card1);
        this.makePurchase(p6, card11);
        this.makePurchase(p8, card2);
        this.makePurchase(p9, card3);
        this.makePurchase(p10, card4);
        this.makePurchase(p11, card5);
        this.makePurchase(p12, card6);
        this.makePurchase(p13, card7);
        this.makePurchase(p14, card8);
        this.makePurchase(p15, card9);
        this.makePurchase(p16, card10);
        this.makePurchase(p17, card11);
        this.makePurchase(p18, card2);
        this.makePurchase(p19, card5);
        this.makePurchase(p20, card8);
        this.makePurchase(p21, card6);
        this.makePurchase(p22, card2);
        this.makePurchase(p23, card8);
        this.makePurchase(p24, card9);
        this.makePurchase(p25, card10);
        this.makePurchase(p26, card3);
        this.makePurchase(p27, card4);
        this.makePurchase(p28, card5);
        this.makePurchase(p29, card9);
        this.makePurchase(p30, card1);
        this.makePurchase(p31, card);
    }
    
    
    
    public void makePurchase(Purchase p, Card c){       
        cards.add(c);
        purchases.add(p);  
        c.addBalance(p.cat.amount);
        c.addPoints(p.cat.amount);
    }  
    
    
    
    // Get user input on which report to generate.
    public void run() {
        System.out.println("Enter Report Type ");
        System.out.println("OPTIONS: points, sales, custom");
        userChoice = input.next();

        if (userChoice.equals("points")) {
            
            Report cardReport = new Report(cards);
            cardReport.type = "Card";
            cardReport.printReport();


        }
        else if (userChoice.equals("sales")) {
            
            Report catReport = new Report(purchases);
            catReport.type = "Purchase";
            catReport.printReport();

        }
        else if (userChoice.equals("custom")) {
            
            Report customReport = new Report(cards);
            customReport.type = "Custom";
            customReport.printReport();

        } else {
            System.out.println();
            System.out.println("Choose from the following options.");
            run();
        }
    }
    
    
    // Base function to populate and then start query.
    public void init(String mode) {
        if (mode == "test") {
            this.populateDev();
            this.run();
        }
        else if (mode == "shop"){
            this.populate();
            this.run();
        }

    }
    
}
